package com.lululemon.commercetools.service;

import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.models.cart.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartService {

    @Autowired
    private ProjectApiRoot apiRoot;

    public Cart createCartWithItem(String currency,String productId, Long quantity){

        CartDraft cartDraft = CartDraft.builder()
                .currency(currency)
                .build();

        Cart cart =  apiRoot.carts()
                .post(cartDraft)
                .executeBlocking()
                .getBody();

        CartAddLineItemAction addLineItem = CartAddLineItemAction.builder()
                .productId(productId)
                .quantity(quantity)
                .build();

        CartUpdate cartUpdate = CartUpdate.builder()
                .version(cart.getVersion())
                .plusActions(addLineItem)
                .build();

        return apiRoot.carts()
                .withId(cart.getId())
                .post(cartUpdate)
                .executeBlocking()
                .getBody();

    }


}
