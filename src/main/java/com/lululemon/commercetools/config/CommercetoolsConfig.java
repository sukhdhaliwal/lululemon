package com.lululemon.commercetools.config;

import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.defaultconfig.ApiRootBuilder;
import com.commercetools.api.defaultconfig.ServiceRegion;
import io.vrap.rmf.base.client.oauth2.ClientCredentials;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class CommercetoolsConfig {

    @Value("${commerce.tools.clientId}")
    private String clientId;

    @Value("${commerce.tools.clientSecret}")
    private String clientSecret;

    @Value("${commerce.tools.projectKey}")
    private String projectKey;

    @Bean
    public ProjectApiRoot apiRoot() {
        log.info("clientId  - {}",clientId);
        log.info("clientSecret - {}",clientSecret);
        log.info("projectKey - {}",projectKey);
        return ApiRootBuilder.of()
                .defaultClient(ClientCredentials.of()
                                .withClientId(clientId)
                                .withClientSecret(clientSecret)
                                .build(),
                        ServiceRegion.AWS_US_EAST_2)
                .build(projectKey);
    }
}

