package com.lululemon.commercetools.controller;

import com.commercetools.api.models.cart.Cart;
import io.micrometer.core.annotation.Timed;
import com.lululemon.commercetools.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @Timed(value = "createCart", description = "create cart with an item in it.")
    @PostMapping("/create-with-item")
    public ResponseEntity<Cart> createCart(@RequestParam String currency, @RequestParam String productId, @RequestParam Long quantity){
        Cart cart = cartService.createCartWithItem(currency,productId,quantity);
        return new ResponseEntity<>(cart, HttpStatus.CREATED);
    }
}
